Sutty Playbook
--------------

Base Ansible playbook for Sutty.

Conventions
-----------

A playbook should be snake-cased and prefixed with `sutty_`.

Playbook name and host group should be the same.

Repository name should also be sufixed with `_playbook`.

Commits should use [Convential
Commits](https://conventionalcommits.org/).

Start a playbook
----------------

Fork this repository and name it after the playbook, ie: "example".

```bash
git clone https://0xacab.org/sutty/ansible/playbook.git sutty_example_playbook
cd sutty_example
git remote rename origin upstream
git remote add origin git@0xacab.org:sutty/ansible/sutty_example_playbook.git
```

Rename the playbook:

```bash
git mv playbook.yml sutty_example.yml
```

Edit `sutty_example.yml` to change "playbook" into the actual name.

```diff
-- hosts: "playbook"
+- hosts: "sutty_example"
```

Do the same for `inventory.yml`:

```diff
-playbook:
-sutty_example:
```

Inventory
---------

Create, link or clone an `inventory.yml` file with the host roles.

```yaml
---
sutty_example:
  hosts:
    host.name:
    host2.name:
```

Host vars
---------

Create, link or clone a directory `host_vars/` with a YAML file for each
host, containing all host-specific variables.

On `host_vars/host.name.yml`:

```yaml
---
custom_variable: "value"
```

Roles
-----

Roles can be added as submodules under the `roles/` directory:

```bash
git submodule add https://0xacab.org/sutty/ansible/sutty_example_role.git roles/sutty_example_role
```
